package com.sharding.order.constant;

/**
 * @author ruyuan
 * @Description 订单常量常量
 */
public class OrderConstant {

    /**
     * 商户店铺状态
     */
    public interface OrderBaseStatus {
        /**
         * 待付款
         */
        int OBLIGATION = 10;

        /**
         * 取消订单
         */
        int CANCEL_ORDER = 70;

    }

    /**
     * 订单key前缀
     */
    public interface OrderRedisKey{
        String PREFIX_KEY = "order:";
    }


}
