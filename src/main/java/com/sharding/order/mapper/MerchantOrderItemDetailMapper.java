package com.sharding.order.mapper;

import com.sharding.order.domain.dto.OrderItemDetailDto;
import com.sharding.order.domain.entity.OrderItemDetail;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author ruyuan
 * 商家订单明细mapper
 */
@Mapper
public interface MerchantOrderItemDetailMapper {

    /**
     * 获取订单详情
     *
     * @param orderNo
     * @return
     */
    List<OrderItemDetailDto> getOrderItemDetailList(@Param("orderNo") String orderNo);

    /**
     * 插入订单明细
     *
     * @param record 入参
     * @return 出参
     */
    int insertSelective(@Param("record") OrderItemDetail record);

    /**
     * 批量插入订单明细
     *
     * @param records 入参
     * @return 出参
     */
    int batchInsert(@Param("records") List<OrderItemDetail> records);


}