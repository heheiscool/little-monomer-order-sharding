package com.sharding.order.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sharding.order.bean.request.order.UserOrderInfoRequest;
import com.sharding.order.bean.web.DataResponse;
import com.sharding.order.bean.web.OperationResponse;
import com.sharding.order.bean.web.PageResponse;
import com.sharding.order.common.enums.OrderCode;
import com.sharding.order.domain.query.UserOrderInfoQuery;
import com.sharding.order.domain.vo.OrderDetailVO;
import com.sharding.order.domain.vo.UserOrderVO;
import com.sharding.order.exception.BaseException;
import com.sharding.order.service.user.UserOrderInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author ruyuan
 * @Description
 */
@Slf4j
@RestController
@RequestMapping(value = "/user/order")
public class UserOrderController {

    @Autowired
    private UserOrderInfoService userOrderInfoService;


    /**
     * 创建订单
     *
     * @param userOrderInfoRequest 入参
     * @return OperationResponse 出参
     */
    @PostMapping("/generateOrder")
    public OperationResponse generateOrder(@RequestBody UserOrderInfoRequest userOrderInfoRequest) {
        try {
            long startTime = System.currentTimeMillis();
            userOrderInfoService.generateOrder(userOrderInfoRequest);
            long endTime = System.currentTimeMillis();
            log.info("创建用户订单耗时：[{}]" , (endTime - startTime));
            return OperationResponse.success(OrderCode.ADD_ORDER_SUCCESS.getDesc());
        } catch (BaseException e) {
            return OperationResponse.error(e.getMessage());
        } catch (Exception e) {
            log.error("generateOrder error: [{}]", e.getMessage(), e);
            return OperationResponse.error(OrderCode.ADD_ORDER_ERROR.getDesc());
        }
    }

    /**
     * 查询订单列表
     *
     * @param userOrderInfoQuery 入参
     * @return PageResponse 出参
     */
    @GetMapping("/queryUserOrderInfoList")
    public PageResponse queryUserOrderInfoList(@RequestBody UserOrderInfoQuery userOrderInfoQuery) {
        try {
            long startTime = System.currentTimeMillis();
            Page<UserOrderVO> userOrderVOPage = userOrderInfoService.queryUserOrderInfoList(userOrderInfoQuery);
            long endTime = System.currentTimeMillis();
            log.info("查询用户订单耗时：[{}]", (endTime - startTime));
            return PageResponse.success(userOrderVOPage);
        } catch (BaseException e) {
            return PageResponse.error(e.getMessage());
        } catch (Exception e) {
            log.error("queryOrderInfoList error: [{}]", e.getMessage(), e);
            return PageResponse.error(e.getMessage());
        }


    }

    /**
     * 根据订单号获得订单明细
     *
     * @param orderNo 入参
     * @reture
     */
    @GetMapping("getOrderDetail")
    public DataResponse getOrderDetail(@RequestParam("orderNo") String orderNo) {
        try {
            long startTime = System.currentTimeMillis();
            OrderDetailVO orderDetailVOList = userOrderInfoService.getOrderDetail(orderNo);
            long endTime = System.currentTimeMillis();
            log.info("查询用户订单明细耗时：[{}]" , (endTime - startTime));
            return DataResponse.success(orderDetailVOList);
        } catch (BaseException e) {
            return DataResponse.error(e.getMessage());
        } catch (Exception e) {
            log.error("getOrderDetail error: [{}]", e.getMessage(), e);
            return DataResponse.error(OrderCode.QUERY_ORDER_ERROR.getDesc());
        }

    }

    /**
     * 取消订单
     * @param orderNo 入参
     * @return OperationResponse 出参
     */
    @PostMapping("/cancelOrder")
    public OperationResponse cancelOrder(@RequestParam("orderNo") String orderNo) {
        try {
            long startTime = System.currentTimeMillis();
            userOrderInfoService.cancelOrder(orderNo);
            long endTime = System.currentTimeMillis();
            log.info("取消用户订单耗时：[{}]" ,(endTime - startTime));
            return OperationResponse.success(OrderCode.DELETE_ORDER_SUCCESS.desc);
        } catch (Exception e) {
            log.error("getOrderDetail getOrderDetail error: [{}]", e.getMessage(), e);
            return OperationResponse.error(OrderCode.DELETE_ORDER_ERROR.getDesc());
        }
    }

}
