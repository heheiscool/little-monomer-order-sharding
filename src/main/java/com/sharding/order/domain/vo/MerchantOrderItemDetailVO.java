package com.sharding.order.domain.vo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author ruyuan
 * @Description
 */
@Data
public class MerchantOrderItemDetailVO implements Serializable {
    /**
     * 订单id
     */
    private String id;
    /**
     * 订单号
     */
    private String orderNo;
    /**
     * 订单金额
     */
    private BigDecimal orderAmount;
    /**
     * 订单状态,10待付款，20待接单，30已接单，40配送中，50已完成，55部分退款，60全部退款，70取消订单'
     */
    private Integer orderStatus;
    /**
     *
     *'交易时间'
     */
    private Date transTime;
    /**
     *
     * 支付状态,1待支付,2支付成功
     */
    private Integer payStatus;
    /**
     *
     * 支付完成时间
     */
    private Date rechargeTime;
    /**
     *
     * 收货地址ID
     */
    private Long addressId;
    /**
     *
     * 订单备注留言
     */
    private String remark;
    /**
     *配送方式，1自提。2配送
     */
    private Integer deliveryType;

    /**
     *配送状态，0待收货，1已收货配送中，2已收货，已送达',
     */
    private Integer deliveryStatus;

    /**
     *优惠券id
     */
    private Long couponId;

    /**
     *商品ID
     */
    private Long productId;

    /**
     *商品购买数量
     */
    private BigDecimal goodsNum;

    /**
     *商品单价
     */
    private BigDecimal goodsPrice;

    /**
     *商品总价
     */
    private BigDecimal goodsAmount;

    /**
     *商品优惠金额
     */
    private BigDecimal discountAmount;

    /**
     *参与活动ID
     */
    private Long discountId;
}
